EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8299 5843
encoding utf-8
Sheet 1 1
Title "Engraver spindle motor drive circuit"
Date "2020-12-27"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Motor:Motor_DC M1
U 1 1 5FFA8724
P 4275 1500
F 0 "M1" H 4433 1496 50  0001 L CNN
F 1 "Motor_DC" H 4433 1405 50  0001 L CNN
F 2 "" H 4275 1410 50  0001 C CNN
F 3 "~" H 4275 1410 50  0001 C CNN
	1    4275 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_DGS T2
U 1 1 5FFB0024
P 4175 2250
F 0 "T2" H 4375 2250 50  0000 L CNN
F 1 "Q_NMOS_DGS" H 4379 2205 50  0001 L CNN
F 2 "" H 4375 2350 50  0001 C CNN
F 3 "~" H 4175 2250 50  0001 C CNN
	1    4175 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0142
U 1 1 5FFBD677
P 4275 3325
F 0 "#PWR0142" H 4275 3075 50  0001 C CNN
F 1 "GND" H 4280 3152 50  0000 C CNN
F 2 "" H 4275 3325 50  0001 C CNN
F 3 "" H 4275 3325 50  0001 C CNN
	1    4275 3325
	1    0    0    -1  
$EndComp
Text GLabel 2775 2950 0    50   Input ~ 0
IO33
$Comp
L Device:D D1
U 1 1 5FFC1ED1
P 3850 1550
F 0 "D1" V 3850 1625 50  0000 L CNN
F 1 "P1000B" V 3895 1629 50  0001 L CNN
F 2 "" H 3850 1550 50  0001 C CNN
F 3 "~" H 3850 1550 50  0001 C CNN
	1    3850 1550
	0    1    1    0   
$EndComp
Connection ~ 4275 1300
$Comp
L power:+48V #PWR0143
U 1 1 60012DF0
P 4275 1150
F 0 "#PWR0143" H 4275 1000 50  0001 C CNN
F 1 "+48V" H 4290 1323 50  0000 C CNN
F 2 "" H 4275 1150 50  0001 C CNN
F 3 "" H 4275 1150 50  0001 C CNN
	1    4275 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4275 1150 4275 1300
$Comp
L Transistor_BJT:2N2219 T1
U 1 1 5FEF355E
P 3300 2950
F 0 "T1" H 3475 2950 50  0000 L CNN
F 1 "2N2219" H 3490 2905 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 3500 2875 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 3300 2950 50  0001 L CNN
	1    3300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2750 3400 2675
Wire Wire Line
	3400 1300 3400 1475
Wire Wire Line
	2775 2950 3100 2950
Wire Wire Line
	4275 1800 4275 1900
Wire Wire Line
	3975 2250 3400 2250
Wire Wire Line
	3400 2250 3400 2375
Wire Wire Line
	3400 1775 3400 2250
Connection ~ 3400 2250
Wire Wire Line
	3400 3150 4275 3150
Wire Wire Line
	4275 2450 4275 3150
Wire Wire Line
	4275 3325 4275 3150
Connection ~ 4275 3150
$Comp
L Device:C C2
U 1 1 5FF4664C
P 5175 2250
F 0 "C2" H 5275 2250 50  0000 L CNN
F 1 "100 nF" H 5175 2150 50  0000 L CNN
F 2 "" H 5213 2100 50  0001 C CNN
F 3 "~" H 5175 2250 50  0001 C CNN
	1    5175 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5175 2100 5175 1300
$Comp
L Device:CP C1
U 1 1 5FF4BBF5
P 4725 2250
F 0 "C1" H 4825 2250 50  0000 L CNN
F 1 "470 uF" H 4725 2150 50  0000 L CNN
F 2 "" H 4763 2100 50  0001 C CNN
F 3 "~" H 4725 2250 50  0001 C CNN
	1    4725 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1300 3850 1300
Wire Wire Line
	3850 1400 3850 1300
Connection ~ 3850 1300
Wire Wire Line
	3850 1300 4275 1300
Wire Wire Line
	4275 1900 3850 1900
Wire Wire Line
	3850 1700 3850 1900
Connection ~ 4275 1900
Wire Wire Line
	4275 1900 4275 2050
$Comp
L Device:R R2
U 1 1 5FEF5AF7
P 3400 2525
F 0 "R2" H 3470 2571 50  0000 L CNN
F 1 "15kΩ" H 3470 2480 50  0000 L CNN
F 2 "" V 3330 2525 50  0001 C CNN
F 3 "~" H 3400 2525 50  0001 C CNN
	1    3400 2525
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5FEF7E34
P 3400 1625
F 0 "R1" H 3470 1671 50  0000 L CNN
F 1 "9.1kΩ" H 3470 1580 50  0000 L CNN
F 2 "" V 3330 1625 50  0001 C CNN
F 3 "~" H 3400 1625 50  0001 C CNN
	1    3400 1625
	1    0    0    -1  
$EndComp
Wire Wire Line
	4725 1300 5175 1300
Connection ~ 4725 1300
Wire Wire Line
	4275 1300 4725 1300
Wire Wire Line
	4725 1300 4725 2100
Wire Wire Line
	4275 3150 4725 3150
Wire Wire Line
	5175 2400 5175 3150
Wire Wire Line
	4725 2400 4725 3150
Connection ~ 4725 3150
Wire Wire Line
	4725 3150 5175 3150
$EndSCHEMATC
