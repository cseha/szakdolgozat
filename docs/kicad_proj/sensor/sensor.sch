EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8299 5843
encoding utf-8
Sheet 1 1
Title "Engraver sensor circuit"
Date "2020-12-27"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L sensor-rescue:motor_drive-rescue_AH49H-ah49h-noname-rescue-motor_drive-cache AH49H
U 1 1 6009721E
P 4700 2450
F 0 "AH49H" V 4400 2450 50  0001 C CNN
F 1 "AH49H" V 4400 2450 50  0000 C CNN
F 2 "" V 4700 2450 50  0001 C CNN
F 3 "" V 4700 2450 50  0001 C CNN
	1    4700 2450
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5FF1EC62
P 3775 2450
F 0 "J?" H 3693 2125 50  0001 C CNN
F 1 "Csatlaklozo" H 3693 2217 50  0000 C CNN
F 2 "" H 3775 2450 50  0001 C CNN
F 3 "~" H 3775 2450 50  0001 C CNN
	1    3775 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	3975 2350 4125 2350
Wire Wire Line
	4125 2550 3975 2550
Wire Wire Line
	3975 2450 4400 2450
Wire Wire Line
	4125 2350 4125 2300
Wire Wire Line
	4125 2300 4400 2300
Wire Wire Line
	4125 2550 4125 2600
Wire Wire Line
	4125 2600 4400 2600
$EndSCHEMATC
