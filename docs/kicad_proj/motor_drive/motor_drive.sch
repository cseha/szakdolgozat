EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8299 5843
encoding utf-8
Sheet 1 1
Title "Engraver stepper motor drive circuit"
Date "2020-12-27"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5675 2550 5675 2850
Wire Wire Line
	6050 2450 6050 2850
Wire Wire Line
	6425 2350 6425 2850
Wire Wire Line
	5300 2850 5300 2650
Wire Wire Line
	6425 2350 6675 2350
Connection ~ 6425 2350
Wire Wire Line
	6425 2225 6425 2350
Wire Wire Line
	6050 2450 6675 2450
Connection ~ 6050 2450
Wire Wire Line
	6050 2225 6050 2450
Wire Wire Line
	5675 2550 6675 2550
Connection ~ 5675 2550
Wire Wire Line
	5675 2225 5675 2550
Wire Wire Line
	5300 2650 6675 2650
Connection ~ 5300 2650
Wire Wire Line
	5300 2225 5300 2650
Wire Wire Line
	4625 2450 6050 2450
Wire Wire Line
	4525 2550 5675 2550
Wire Wire Line
	4425 2650 5300 2650
Wire Wire Line
	4625 2150 4625 2450
Wire Wire Line
	4525 2300 4525 2550
Wire Wire Line
	4425 2450 4425 2650
Wire Wire Line
	4150 2150 4625 2150
Wire Wire Line
	4150 2300 4525 2300
Wire Wire Line
	4150 2450 4425 2450
Wire Wire Line
	3300 1250 3300 625 
Wire Wire Line
	3150 1250 3150 700 
Wire Wire Line
	3000 1250 3000 775 
Wire Wire Line
	3450 2950 3450 2850
Wire Wire Line
	3750 2950 3750 2850
Wire Wire Line
	4150 1700 4300 1700
Wire Wire Line
	3375 1150 3375 1025
Wire Wire Line
	3450 1150 3375 1150
Wire Wire Line
	3450 1250 3450 1150
Wire Wire Line
	3675 1150 3675 1025
Wire Wire Line
	3600 1150 3675 1150
Wire Wire Line
	3600 1250 3600 1150
Wire Wire Line
	3300 2850 3300 3450
$Comp
L Device:R R1
U 1 1 5FDC6AED
P 3150 3000
F 0 "R1" H 3200 3050 50  0000 L CNN
F 1 "100kΩ" H 3200 2950 50  0000 L CNN
F 2 "" V 3080 3000 50  0001 C CNN
F 3 "~" H 3150 3000 50  0001 C CNN
	1    3150 3000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2450 3150 3150 3150
Wire Wire Line
	2350 2850 3000 2850
Wire Wire Line
	2550 2450 2250 2450
Wire Wire Line
	2550 2300 2150 2300
Wire Wire Line
	2150 2300 2150 3675
Wire Wire Line
	2550 2150 2050 2150
Wire Wire Line
	2550 2000 1950 2000
Wire Wire Line
	3750 1025 3750 1250
Wire Wire Line
	3600 3300 3750 3300
Connection ~ 4225 3300
Wire Wire Line
	4225 3300 4225 3450
Wire Wire Line
	3600 2850 3600 3300
Wire Wire Line
	3750 3450 3750 3300
Connection ~ 3750 3300
Wire Wire Line
	3750 3300 4225 3300
Wire Wire Line
	4225 3200 4225 3300
Wire Wire Line
	4150 2000 4725 2000
Wire Wire Line
	4725 2350 4725 2000
Wire Wire Line
	4725 2350 6425 2350
Wire Wire Line
	4300 1700 4300 1025
Wire Wire Line
	3875 1025 3750 1025
Wire Wire Line
	4175 1025 4300 1025
Wire Wire Line
	6425 1925 6425 1850
Wire Wire Line
	6425 1850 6050 1850
Wire Wire Line
	5300 1850 5300 1925
Wire Wire Line
	5675 1925 5675 1850
Connection ~ 5675 1850
Wire Wire Line
	5675 1850 5300 1850
Wire Wire Line
	6050 1925 6050 1850
Connection ~ 6050 1850
Wire Wire Line
	6050 1850 5675 1850
Wire Wire Line
	6425 3250 6425 3150
Wire Wire Line
	5300 3150 5300 3250
Wire Wire Line
	5300 3250 5675 3250
Connection ~ 6425 3250
Wire Wire Line
	6050 3150 6050 3250
Connection ~ 6050 3250
Wire Wire Line
	6050 3250 6425 3250
Wire Wire Line
	5675 3150 5675 3250
Connection ~ 5675 3250
Wire Wire Line
	5675 3250 6050 3250
Wire Wire Line
	4225 3750 4225 3825
Wire Wire Line
	3300 3750 3300 3825
Wire Wire Line
	3300 3825 3750 3825
Connection ~ 4225 3825
Wire Wire Line
	3750 3750 3750 3825
Connection ~ 3750 3825
Wire Wire Line
	3750 3825 4225 3825
$Comp
L motor_drive-rescue:DRV8434-drv8434-noname-rescue-motor_drive-rescue Y_DRV
U 1 1 5FDC6B46
P 3350 2050
F 0 "Y_DRV" H 2650 2700 50  0001 L CNB
F 1 "DRV8434" H 3084 2054 79  0000 L CNB
F 2 "" H 3450 3550 50  0001 C CNN
F 3 "" H 3450 3550 50  0001 C CNN
	1    3350 2050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6425 3250 6425 3350
Connection ~ 5300 1850
Wire Wire Line
	5300 1775 5300 1850
Wire Wire Line
	1950 2000 1950 2675
Connection ~ 4300 1700
Connection ~ 5000 1700
Wire Wire Line
	5000 1500 5000 1700
Wire Wire Line
	1850 2475 1850 2800
Wire Wire Line
	1775 2800 1850 2800
Wire Wire Line
	1775 1975 1850 1975
Wire Wire Line
	1575 2475 1850 2475
Wire Wire Line
	1850 3300 1850 3800
Wire Wire Line
	1775 3300 1850 3300
Wire Wire Line
	1775 3800 1850 3800
Wire Wire Line
	1850 4625 1850 4725
Connection ~ 1850 4625
Wire Wire Line
	1775 4625 1850 4625
Wire Wire Line
	1375 3800 1275 3800
Wire Wire Line
	1375 3300 1275 3300
Wire Wire Line
	1275 3300 1275 3800
Wire Wire Line
	1275 4625 1375 4625
Connection ~ 1850 2475
Wire Wire Line
	1850 1975 1850 2475
$Comp
L Device:R R2
U 1 1 6009F654
P 1575 2325
F 0 "R2" H 1625 2375 50  0000 L CNN
F 1 "330kΩ" H 1625 2275 50  0000 L CNN
F 2 "" V 1505 2325 50  0001 C CNN
F 3 "~" H 1575 2325 50  0001 C CNN
	1    1575 2325
	-1   0    0    -1  
$EndComp
Text GLabel 1150 1975 0    50   Input ~ 0
DVDD
$Comp
L power:GND #PWR0114
U 1 1 5FD577BE
P 1850 4725
F 0 "#PWR0114" H 1850 4475 50  0001 C CNN
F 1 "GND" H 2075 4650 50  0000 R CNN
F 2 "" H 1850 4725 50  0001 C CNN
F 3 "" H 1850 4725 50  0001 C CNN
	1    1850 4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	1575 1850 2550 1850
Wire Wire Line
	5000 1775 5000 1700
Wire Wire Line
	5000 2075 5000 2125
Wire Wire Line
	4250 1850 4150 1850
Wire Wire Line
	4300 1700 5000 1700
Wire Wire Line
	1575 2725 1575 2675
Wire Wire Line
	1575 2675 1950 2675
Wire Wire Line
	1575 3225 1575 3175
Wire Wire Line
	1575 3175 2050 3175
Wire Wire Line
	2050 2150 2050 3175
Wire Wire Line
	1575 3675 2150 3675
Wire Wire Line
	1575 4550 1575 4500
Wire Wire Line
	1575 4500 2250 4500
Connection ~ 1275 3300
Connection ~ 1850 2800
Connection ~ 1850 3300
Wire Wire Line
	1575 1900 1575 1850
Wire Wire Line
	1850 2800 1850 3300
Wire Wire Line
	1150 1975 1275 1975
Connection ~ 1275 1975
Wire Wire Line
	1275 1975 1375 1975
Wire Wire Line
	1275 1975 1275 2800
Wire Wire Line
	1375 2800 1275 2800
Connection ~ 1275 2800
Wire Wire Line
	1275 2800 1275 3300
Wire Wire Line
	950  1700 950  1350
Wire Wire Line
	950  1700 2550 1700
Wire Wire Line
	950  775  950  850 
Wire Wire Line
	1050 700  1050 850 
Wire Wire Line
	1150 625  1150 850 
Wire Wire Line
	950  775  3000 775 
Wire Wire Line
	1050 700  3150 700 
Wire Wire Line
	1150 625  3300 625 
Wire Wire Line
	1050 1350 1050 1600
Wire Wire Line
	1050 1600 2350 1600
Wire Wire Line
	2350 1600 2350 2850
Wire Wire Line
	1150 1350 1150 1500
Wire Wire Line
	1150 1500 2450 1500
Wire Wire Line
	2450 1500 2450 3150
Wire Wire Line
	1575 3725 1575 3675
Connection ~ 1850 3800
Wire Wire Line
	1275 3800 1275 4625
Connection ~ 1275 3800
Wire Wire Line
	2250 2450 2250 4500
Wire Wire Line
	1575 4300 1850 4300
Wire Wire Line
	1850 3800 1850 4300
Connection ~ 1850 4300
Wire Wire Line
	1850 4300 1850 4625
$Comp
L Connector_Generic:Conn_02x03_Counter_Clockwise asd
U 1 1 605DBA57
P 1050 1050
F 0 "asd" V 1054 1230 50  0001 L CNN
F 1 "MCU_csatlakozo" V 1100 1225 50  0000 L CNN
F 2 "" H 1050 1050 50  0001 C CNN
F 3 "~" H 1050 1050 50  0001 C CNN
	1    1050 1050
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 6063820C
P 6875 2450
F 0 "J?" H 6955 2442 50  0001 L CNN
F 1 "Motor_csatlakozo" H 6955 2396 50  0000 L CNN
F 2 "" H 6875 2450 50  0001 C CNN
F 3 "~" H 6875 2450 50  0001 C CNN
	1    6875 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 5FDC6AE3
P 4225 3825
F 0 "#PWR0123" H 4225 3575 50  0001 C CNN
F 1 "GND" H 4225 3675 50  0000 C CNN
F 2 "" H 4225 3825 50  0001 C CNN
F 3 "" H 4225 3825 50  0001 C CNN
	1    4225 3825
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FEFE2DE
P 6425 3350
F 0 "#PWR?" H 6425 3100 50  0001 C CNN
F 1 "GND" H 6430 3177 50  0000 C CNN
F 2 "" H 6425 3350 50  0001 C CNN
F 3 "" H 6425 3350 50  0001 C CNN
	1    6425 3350
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5FDC6AA3
P 3450 2950
F 0 "#PWR0120" H 3450 2700 50  0001 C CNN
F 1 "GND" H 3450 2800 50  0000 C CNN
F 2 "" H 3450 2950 50  0001 C CNN
F 3 "" H 3450 2950 50  0001 C CNN
	1    3450 2950
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5FDC6A99
P 3750 2950
F 0 "#PWR0119" H 3750 2700 50  0001 C CNN
F 1 "GND" H 3750 2800 50  0000 C CNN
F 2 "" H 3750 2950 50  0001 C CNN
F 3 "" H 3750 2950 50  0001 C CNN
	1    3750 2950
	-1   0    0    -1  
$EndComp
$Comp
L power:+48V #PWR?
U 1 1 60454029
P 4225 3200
F 0 "#PWR?" H 4225 3050 50  0001 C CNN
F 1 "+48V" H 4240 3373 50  0000 C CNN
F 2 "" H 4225 3200 50  0001 C CNN
F 3 "" H 4225 3200 50  0001 C CNN
	1    4225 3200
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5FDC6A53
P 4250 1850
F 0 "#PWR0116" H 4250 1600 50  0001 C CNN
F 1 "GND" V 4255 1722 50  0000 R CNN
F 2 "" H 4250 1850 50  0001 C CNN
F 3 "" H 4250 1850 50  0001 C CNN
	1    4250 1850
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5FDC6A5D
P 5000 2125
F 0 "#PWR0117" H 5000 1875 50  0001 C CNN
F 1 "GND" H 5005 1952 50  0000 C CNN
F 2 "" H 5000 2125 50  0001 C CNN
F 3 "" H 5000 2125 50  0001 C CNN
	1    5000 2125
	-1   0    0    -1  
$EndComp
$Comp
L power:+48V #PWR0118
U 1 1 5FDC6A7E
P 5000 1500
F 0 "#PWR0118" H 5000 1350 50  0001 C CNN
F 1 "+48V" H 5015 1673 50  0000 C CNN
F 2 "" H 5000 1500 50  0001 C CNN
F 3 "" H 5000 1500 50  0001 C CNN
	1    5000 1500
	-1   0    0    -1  
$EndComp
$Comp
L power:+48V #PWR?
U 1 1 5FF06D50
P 5300 1775
F 0 "#PWR?" H 5300 1625 50  0001 C CNN
F 1 "+48V" H 5300 1925 50  0000 C CNN
F 2 "" H 5300 1775 50  0001 C CNN
F 3 "" H 5300 1775 50  0001 C CNN
	1    5300 1775
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5FDC6A8B
P 4025 1025
F 0 "C2" V 3975 925 50  0000 C CNN
F 1 "0.22uF" V 3975 1200 50  0000 C CNN
F 2 "" H 4063 875 50  0001 C CNN
F 3 "~" H 4025 1025 50  0001 C CNN
	1    4025 1025
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5FDC6A43
P 3525 1025
F 0 "C1" V 3273 1025 50  0000 C CNN
F 1 "0.022uF" V 3364 1025 50  0000 C CNN
F 2 "" H 3563 875 50  0001 C CNN
F 3 "~" H 3525 1025 50  0001 C CNN
	1    3525 1025
	0    1    1    0   
$EndComp
$Comp
L Device:D D4
U 1 1 5FEF94E9
P 5675 3000
F 0 "D4" V 5629 3079 50  0000 L CNN
F 1 "D" V 5720 3079 50  0001 L CNN
F 2 "" H 5675 3000 50  0001 C CNN
F 3 "~" H 5675 3000 50  0001 C CNN
	1    5675 3000
	0    1    1    0   
$EndComp
$Comp
L Device:D D6
U 1 1 5FE7FE90
P 6050 3000
F 0 "D6" V 6004 3079 50  0000 L CNN
F 1 "D" V 6095 3079 50  0001 L CNN
F 2 "" H 6050 3000 50  0001 C CNN
F 3 "~" H 6050 3000 50  0001 C CNN
	1    6050 3000
	0    1    1    0   
$EndComp
$Comp
L Device:D D8
U 1 1 5FEFE2D4
P 6425 3000
F 0 "D8" V 6379 3079 50  0000 L CNN
F 1 "D" V 6470 3079 50  0001 L CNN
F 2 "" H 6425 3000 50  0001 C CNN
F 3 "~" H 6425 3000 50  0001 C CNN
	1    6425 3000
	0    1    1    0   
$EndComp
$Comp
L Device:D D7
U 1 1 5FF0D236
P 6425 2075
F 0 "D7" V 6379 2154 50  0000 L CNN
F 1 "D" V 6470 2154 50  0001 L CNN
F 2 "" H 6425 2075 50  0001 C CNN
F 3 "~" H 6425 2075 50  0001 C CNN
	1    6425 2075
	0    1    1    0   
$EndComp
$Comp
L Device:D D5
U 1 1 5FF09FB9
P 6050 2075
F 0 "D5" V 6004 2154 50  0000 L CNN
F 1 "D" V 6095 2154 50  0001 L CNN
F 2 "" H 6050 2075 50  0001 C CNN
F 3 "~" H 6050 2075 50  0001 C CNN
	1    6050 2075
	0    1    1    0   
$EndComp
$Comp
L Device:D D3
U 1 1 5FE7FEAE
P 5675 2075
F 0 "D3" V 5629 2154 50  0000 L CNN
F 1 "D" V 5720 2154 50  0001 L CNN
F 2 "" H 5675 2075 50  0001 C CNN
F 3 "~" H 5675 2075 50  0001 C CNN
	1    5675 2075
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5FDC6A73
P 5000 1925
F 0 "C3" H 5050 2000 50  0000 C CNN
F 1 "0.01uF" H 5125 1850 50  0000 C CNN
F 2 "" H 5038 1775 50  0001 C CNN
F 3 "~" H 5000 1925 50  0001 C CNN
	1    5000 1925
	1    0    0    -1  
$EndComp
$Comp
L Device:D D1
U 1 1 5FF06D56
P 5300 2075
F 0 "D1" V 5254 2154 50  0000 L CNN
F 1 "D" V 5345 2154 50  0001 L CNN
F 2 "" H 5300 2075 50  0001 C CNN
F 3 "~" H 5300 2075 50  0001 C CNN
	1    5300 2075
	0    1    1    0   
$EndComp
$Comp
L Device:D D2
U 1 1 5FF03345
P 5300 3000
F 0 "D2" V 5254 3079 50  0000 L CNN
F 1 "D" V 5345 3079 50  0001 L CNN
F 2 "" H 5300 3000 50  0001 C CNN
F 3 "~" H 5300 3000 50  0001 C CNN
	1    5300 3000
	0    1    1    0   
$EndComp
$Comp
L Device:CP C5
U 1 1 604720D8
P 4225 3600
F 0 "C5" H 4343 3646 50  0000 L CNN
F 1 "100uF" H 4343 3555 50  0000 L CNN
F 2 "" H 4263 3450 50  0001 C CNN
F 3 "~" H 4225 3600 50  0001 C CNN
	1    4225 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5FDC6AAD
P 3750 3600
F 0 "C4" H 3825 3675 50  0000 C CNN
F 1 "0.01uF" H 3625 3525 50  0000 C CNN
F 2 "" H 3788 3450 50  0001 C CNN
F 3 "~" H 3750 3600 50  0001 C CNN
	1    3750 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5FDC6AB7
P 3300 3600
F 0 "C6" H 3375 3675 50  0000 C CNN
F 1 "0.47uF" H 3175 3525 50  0000 C CNN
F 2 "" H 3338 3450 50  0001 C CNN
F 3 "~" H 3300 3600 50  0001 C CNN
	1    3300 3600
	1    0    0    -1  
$EndComp
$Comp
L motor_drive-rescue:jump-drv8434-noname-rescue-motor_drive-rescue JP_5
U 1 1 5FFFDDAD
P 1525 4950
F 0 "JP_5" H 1475 4700 50  0000 C CNN
F 1 "JP_5" H 1475 4700 50  0001 C CNN
F 2 "" H 1525 4950 50  0001 C CNN
F 3 "" H 1525 4950 50  0001 C CNN
	1    1525 4950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5FE3DF29
P 1575 4150
F 0 "R3" H 1425 4200 50  0000 L CNN
F 1 "330kΩ" H 1275 4100 50  0000 L CNN
F 2 "" V 1505 4150 50  0001 C CNN
F 3 "~" H 1575 4150 50  0001 C CNN
	1    1575 4150
	1    0    0    -1  
$EndComp
$Comp
L motor_drive-rescue:jumper_4-drv8434-noname-rescue-motor_drive-rescue JP_4
U 1 1 5FE3DF33
P 1525 4125
F 0 "JP_4" H 1675 3875 50  0000 R CNN
F 1 "JP_4" H 1675 3875 50  0001 R CNN
F 2 "" H 1525 4125 50  0001 C CNN
F 3 "" H 1525 4125 50  0001 C CNN
	1    1525 4125
	-1   0    0    1   
$EndComp
$Comp
L motor_drive-rescue:jump-drv8434-noname-rescue-motor_drive-rescue JP_3
U 1 1 6005741F
P 1525 3625
F 0 "JP_3" H 1475 3375 50  0000 C CNN
F 1 "JP_3" H 1475 3375 50  0001 C CNN
F 2 "" H 1525 3625 50  0001 C CNN
F 3 "" H 1525 3625 50  0001 C CNN
	1    1525 3625
	-1   0    0    1   
$EndComp
$Comp
L motor_drive-rescue:jump-drv8434-noname-rescue-motor_drive-rescue JP_2
U 1 1 6003E40F
P 1525 3125
F 0 "JP_2" H 1475 2875 50  0000 C CNN
F 1 "JP_2" H 1475 2875 50  0001 C CNN
F 2 "" H 1525 3125 50  0001 C CNN
F 3 "" H 1525 3125 50  0001 C CNN
	1    1525 3125
	-1   0    0    1   
$EndComp
$Comp
L motor_drive-rescue:jumper_4-drv8434-noname-rescue-motor_drive-rescue JP_1
U 1 1 5FFEA145
P 1525 2300
F 0 "JP_1" H 1675 2050 50  0000 R CNN
F 1 "JP_1" H 1675 2050 50  0001 R CNN
F 2 "" H 1525 2300 50  0001 C CNN
F 3 "" H 1525 2300 50  0001 C CNN
	1    1525 2300
	-1   0    0    1   
$EndComp
$EndSCHEMATC
