EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8299 5843
encoding utf-8
Sheet 1 1
Title "Engraver ESP32 circuit"
Date "2020-12-27"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L mcu-rescue:ESP32-DEVKITC-32D-ESP32-DEVKITC-32D-noname-rescue-motor_drive-rescue U1
U 1 1 5FF0A9C5
P 3675 2475
F 0 "U1" H 3675 3642 50  0001 C CNN
F 1 "ESP32-DEVKITC-32D" H 3675 3551 50  0000 C CNN
F 2 "MODULE_ESP32-DEVKITC-32D" H 3675 2475 50  0001 L BNN
F 3 "" H 3675 2475 50  0001 L BNN
F 4 "Espressif Systems" H 3675 2475 50  0001 L BNN "MANUFACTURER"
F 5 "4" H 3675 2475 50  0001 L BNN "PARTREV"
	1    3675 2475
	1    0    0    -1  
$EndComp
Text GLabel 4475 1675 2    50   Input ~ 0
közös_nSLEEP
Text GLabel 4475 1775 2    50   Input ~ 0
közös_nFAULT
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J?
U 1 1 5FEA4CC8
P 6075 1875
F 0 "J?" H 6125 2192 50  0001 C CNN
F 1 "X motor csatl." H 6125 2100 50  0000 C CNN
F 2 "" H 6075 1875 50  0001 C CNN
F 3 "~" H 6075 1875 50  0001 C CNN
	1    6075 1875
	-1   0    0    -1  
$EndComp
Text GLabel 2875 2375 0    50   Input ~ 0
közös_VREF
Text GLabel 2875 2875 0    50   Input ~ 0
GND
NoConn ~ 2875 2075
NoConn ~ 2875 1975
NoConn ~ 2875 1875
NoConn ~ 2875 1775
NoConn ~ 2875 1675
NoConn ~ 2875 2475
NoConn ~ 2875 3075
NoConn ~ 2875 3175
NoConn ~ 2875 3275
NoConn ~ 2875 3375
NoConn ~ 4475 3375
NoConn ~ 4475 3275
NoConn ~ 4475 3175
NoConn ~ 4475 3075
NoConn ~ 4475 2975
NoConn ~ 4475 2875
NoConn ~ 4475 1975
NoConn ~ 4475 1875
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5FEA6353
P 1775 2200
F 0 "J?" H 1693 1875 50  0001 C CNN
F 1 "X szenzor csatl." H 1525 2000 50  0000 C CNN
F 2 "" H 1775 2200 50  0001 C CNN
F 3 "~" H 1775 2200 50  0001 C CNN
	1    1775 2200
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 SPWM
U 1 1 5FEA6F55
P 1775 1825
F 0 "SPWM" H 1725 1725 50  0001 C CNN
F 1 "SPWM" H 1700 1725 50  0000 C CNN
F 2 "" H 1775 1825 50  0001 C CNN
F 3 "~" H 1775 1825 50  0001 C CNN
	1    1775 1825
	-1   0    0    1   
$EndComp
$Comp
L power:GND GND
U 1 1 5FECFA9B
P 1975 2575
F 0 "GND" V 1975 2375 50  0000 C CNN
F 1 "GND" V 1975 2425 50  0001 R CNN
F 2 "" H 1975 2575 50  0001 C CNN
F 3 "" H 1975 2575 50  0001 C CNN
	1    1975 2575
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V +3.3V
U 1 1 5FED0DC5
P 1975 3250
F 0 "+3.3V" V 1975 3475 50  0000 C CNN
F 1 "+3.3V" V 1990 3378 50  0001 L CNN
F 2 "" H 1975 3250 50  0001 C CNN
F 3 "" H 1975 3250 50  0001 C CNN
	1    1975 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	2350 2200 1975 2200
Wire Wire Line
	2350 3150 1975 3150
NoConn ~ 2875 2175
Text GLabel 4475 2075 2    50   Input ~ 0
közös_STEP
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J?
U 1 1 5FEA67E6
P 6075 3025
F 0 "J?" H 6125 3342 50  0001 C CNN
F 1 "Z motor csatl." H 6125 3250 50  0000 C CNN
F 2 "" H 6075 3025 50  0001 C CNN
F 3 "~" H 6075 3025 50  0001 C CNN
	1    6075 3025
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J?
U 1 1 5FEA65D9
P 6075 2475
F 0 "J?" H 6125 2792 50  0001 C CNN
F 1 "Y motor csatl." H 6125 2700 50  0000 C CNN
F 2 "" H 6075 2475 50  0001 C CNN
F 3 "~" H 6075 2475 50  0001 C CNN
	1    6075 2475
	-1   0    0    -1  
$EndComp
Text GLabel 6275 1775 2    50   Input ~ 0
közös_STEP
Text GLabel 6275 2375 2    50   Input ~ 0
közös_STEP
Text GLabel 6275 2925 2    50   Input ~ 0
közös_STEP
Text GLabel 6275 1975 2    50   Input ~ 0
közös_nFAULT
Text GLabel 6275 2575 2    50   Input ~ 0
közös_nFAULT
Text GLabel 6275 3125 2    50   Input ~ 0
közös_nFAULT
Text GLabel 6275 3025 2    50   Input ~ 0
közös_VREF
Text GLabel 6275 2475 2    50   Input ~ 0
közös_VREF
Text GLabel 6275 1875 2    50   Input ~ 0
közös_VREF
Text GLabel 5775 1775 0    50   Input ~ 0
közös_nSLEEP
Text GLabel 5775 2375 0    50   Input ~ 0
közös_nSLEEP
Text GLabel 5775 2925 0    50   Input ~ 0
közös_nSLEEP
Wire Wire Line
	5100 2375 5100 1975
Wire Wire Line
	5100 1975 5775 1975
Wire Wire Line
	5100 2675 5100 3025
Wire Wire Line
	5100 3025 5775 3025
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5FEA61E6
P 1775 2675
F 0 "J?" H 1693 2350 50  0001 C CNN
F 1 "Y szenzor csatl." H 1525 2475 50  0000 C CNN
F 2 "" H 1775 2675 50  0001 C CNN
F 3 "~" H 1775 2675 50  0001 C CNN
	1    1775 2675
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5FEA5EC5
P 1775 3150
F 0 "J?" H 1855 3192 50  0001 L CNN
F 1 "Z szenzor csatl." H 1525 2950 50  0000 C CNN
F 2 "" H 1775 3150 50  0001 C CNN
F 3 "~" H 1775 3150 50  0001 C CNN
	1    1775 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	2350 2200 2350 2575
Wire Wire Line
	2350 2775 2350 3150
Text Notes 3675 2300 0    31   ~ 0
X motor ENALBE 
Text Notes 3600 2400 0    31   ~ 0
X motor DIRECTION
Text Notes 3675 2500 0    31   ~ 0
Y motor ENALBE 
Text Notes 3675 2700 0    31   ~ 0
Z motor ENALBE 
Text Notes 3600 2600 0    31   ~ 0
Y motor DIRECTION
Text Notes 3600 2800 0    31   ~ 0
Z motor DIRECTION
Text GLabel 4475 1575 2    50   Input ~ 0
GND
Text GLabel 4475 2175 2    50   Input ~ 0
GND
Text GLabel 2875 1575 0    50   Input ~ 0
X-Y-Z_szenzor
Wire Wire Line
	2475 1825 2475 2275
Wire Wire Line
	1975 1825 2475 1825
$Comp
L power:GND GND
U 1 1 6002AE03
P 1975 2100
F 0 "GND" V 1975 1900 50  0000 C CNN
F 1 "GND" V 1975 1950 50  0001 R CNN
F 2 "" H 1975 2100 50  0001 C CNN
F 3 "" H 1975 2100 50  0001 C CNN
	1    1975 2100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND GND
U 1 1 6002B164
P 1975 3050
F 0 "GND" V 1975 2850 50  0000 C CNN
F 1 "GND" V 1975 2900 50  0001 R CNN
F 2 "" H 1975 3050 50  0001 C CNN
F 3 "" H 1975 3050 50  0001 C CNN
	1    1975 3050
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V +3.3V
U 1 1 6002C219
P 1975 2775
F 0 "+3.3V" V 1975 3000 50  0000 C CNN
F 1 "+3.3V" V 1990 2903 50  0001 L CNN
F 2 "" H 1975 2775 50  0001 C CNN
F 3 "" H 1975 2775 50  0001 C CNN
	1    1975 2775
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V +3.3V
U 1 1 6002C5D6
P 1975 2300
F 0 "+3.3V" V 1975 2525 50  0000 C CNN
F 1 "+3.3V" V 1990 2428 50  0001 L CNN
F 2 "" H 1975 2300 50  0001 C CNN
F 3 "" H 1975 2300 50  0001 C CNN
	1    1975 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	2475 2275 2875 2275
Wire Wire Line
	2350 2575 2875 2575
Wire Wire Line
	2350 2775 2875 2775
Wire Wire Line
	1975 2675 2875 2675
Wire Wire Line
	5000 1875 5775 1875
Wire Wire Line
	5000 3125 5775 3125
Wire Wire Line
	5100 2375 4475 2375
Wire Wire Line
	4475 2275 5000 2275
Wire Wire Line
	5000 2275 5000 1875
Wire Wire Line
	5100 2675 4475 2675
Wire Wire Line
	4475 2775 5000 2775
Wire Wire Line
	5000 2775 5000 3125
Wire Wire Line
	5775 2575 4475 2575
Wire Wire Line
	4475 2475 5775 2475
NoConn ~ 2875 2975
$EndSCHEMATC
