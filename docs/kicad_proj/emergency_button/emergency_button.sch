EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8299 5843
encoding utf-8
Sheet 1 1
Title "Emergency button circuit"
Date "2020-12-27"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4550 2475 4550 2600
Wire Wire Line
	4475 2475 4550 2475
$Comp
L power:GND #PWR?
U 1 1 60031779
P 4550 2600
F 0 "#PWR?" H 4550 2350 50  0001 C CNN
F 1 "GND" H 4555 2427 50  0000 C CNN
F 2 "" H 4550 2600 50  0001 C CNN
F 3 "" H 4550 2600 50  0001 C CNN
	1    4550 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2475 3875 2475
Wire Wire Line
	3800 2350 3800 2475
$Comp
L power:+3.3V #PWR?
U 1 1 60031771
P 3800 2350
F 0 "#PWR?" H 3800 2200 50  0001 C CNN
F 1 "+3.3V" H 3815 2523 50  0000 C CNN
F 2 "" H 3800 2350 50  0001 C CNN
F 3 "" H 3800 2350 50  0001 C CNN
	1    3800 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4175 2100 4175 2275
Wire Wire Line
	4050 2100 4175 2100
Text GLabel 4050 2100 0    50   Input ~ 0
IO13
$Comp
L Analog_Switch:DG417LDJ_Maxim U?
U 1 1 60031768
P 4175 2475
F 0 "U?" H 4175 2650 50  0001 C CNN
F 1 "EMERGENCY_STOP" H 4175 2650 50  0000 C CNB
F 2 "Package_DIP:DIP-8_W7.62mm" H 4175 2375 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/DG417L-DG419L.pdf" H 4175 2475 50  0001 C CNN
	1    4175 2475
	-1   0    0    1   
$EndComp
$EndSCHEMATC
