EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1000 2000 0    50   Input ~ 0
A
Text GLabel 4000 2000 2    50   Input ~ 0
B
$Comp
L Motor:Stepper_Motor_bipolar motor
U 1 1 5FBE44A4
P 2500 2300
F 0 "motor" H 2688 2378 50  0001 L CNN
F 1 "Stepper_Motor_bipolar" H 2688 2333 50  0001 L CNN
F 2 "" H 2510 2290 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 2510 2290 50  0001 C CNN
	1    2500 2300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC160 T4
U 1 1 5FBE62FF
P 3600 1500
F 0 "T4" H 3791 1500 50  0000 L CNB
F 1 "BC160" H 3791 1545 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 3800 1425 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/1697389.pdf" H 3600 1500 50  0001 L CNN
	1    3600 1500
	-1   0    0    1   
$EndComp
$Comp
L Simulation_SPICE:DIODE D4
U 1 1 5FBE8C05
P 3000 1400
F 0 "D4" V 3000 1320 50  0000 R CNB
F 1 "DIODE" V 2955 1320 50  0001 R CNN
F 2 "" H 3000 1400 50  0001 C CNN
F 3 "~" H 3000 1400 50  0001 C CNN
F 4 "Y" H 3000 1400 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 3000 1400 50  0001 L CNN "Spice_Primitive"
	1    3000 1400
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:BC160 T1
U 1 1 5FBE83BE
P 1400 1500
F 0 "T1" H 1590 1500 50  0000 L CNB
F 1 "BC160" H 1591 1545 50  0001 L CNN
F 2 "" H 1600 1425 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/1697389.pdf" H 1400 1500 50  0001 L CNN
	1    1400 1500
	1    0    0    1   
$EndComp
$Comp
L Simulation_SPICE:DIODE D2
U 1 1 5FBFA2D9
P 2000 2500
F 0 "D2" V 2000 2420 50  0000 R CNB
F 1 "DIODE" V 1955 2420 50  0001 R CNN
F 2 "" H 2000 2500 50  0001 C CNN
F 3 "~" H 2000 2500 50  0001 C CNN
F 4 "Y" H 2000 2500 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2000 2500 50  0001 L CNN "Spice_Primitive"
	1    2000 2500
	0    -1   -1   0   
$EndComp
$Comp
L Simulation_SPICE:DIODE D3
U 1 1 5FBFA4DF
P 3000 2500
F 0 "D3" V 3000 2420 50  0000 R CNB
F 1 "DIODE" V 2955 2420 50  0001 R CNN
F 2 "" H 3000 2500 50  0001 C CNN
F 3 "~" H 3000 2500 50  0001 C CNN
F 4 "Y" H 3000 2500 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 3000 2500 50  0001 L CNN "Spice_Primitive"
	1    3000 2500
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:BC107 T2
U 1 1 5FBE7D1F
P 1400 2500
F 0 "T2" H 1590 2500 50  0000 L CNB
F 1 "BC107" H 1591 2455 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-18-3" H 1600 2425 50  0001 L CNN
F 3 "http://www.b-kainka.de/Daten/Transistor/BC108.pdf" H 1400 2500 50  0001 L CNN
	1    1400 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF GND
U 1 1 5FBE5530
P 2500 3100
F 0 "GND" H 2588 3063 50  0001 L CNN
F 1 "GNDREF" H 2505 2927 50  0001 C CNN
F 2 "" H 2500 3100 50  0001 C CNN
F 3 "" H 2500 3100 50  0001 C CNN
	1    2500 3100
	1    0    0    -1  
$EndComp
$Comp
L power:+1V2 +U
U 1 1 5FC2AA94
P 2500 900
F 0 "+U" H 2588 937 50  0000 L CNB
F 1 "+1V2" H 2515 1073 50  0001 C CNN
F 2 "" H 2500 900 50  0001 C CNN
F 3 "" H 2500 900 50  0001 C CNN
	1    2500 900 
	1    0    0    -1  
$EndComp
NoConn ~ 2200 2200
NoConn ~ 2200 2400
Wire Wire Line
	2500 900  2500 1000
Wire Wire Line
	2500 1000 2000 1000
Wire Wire Line
	1500 1000 1500 1300
Wire Wire Line
	2500 1000 3000 1000
Wire Wire Line
	3500 1000 3500 1300
Connection ~ 2500 1000
Wire Wire Line
	3000 1000 3000 1250
Connection ~ 3000 1000
Wire Wire Line
	3000 1000 3500 1000
Wire Wire Line
	2000 1000 2000 1250
Connection ~ 2000 1000
Wire Wire Line
	2000 1000 1500 1000
Wire Wire Line
	2000 1550 2000 2000
Wire Wire Line
	3000 1550 3000 2000
Wire Wire Line
	3500 1700 3500 2000
Wire Wire Line
	2500 3100 2500 3000
Wire Wire Line
	2500 3000 2000 3000
Wire Wire Line
	1500 3000 1500 2700
Wire Wire Line
	2500 3000 3000 3000
Wire Wire Line
	3500 3000 3500 2700
Connection ~ 2500 3000
Wire Wire Line
	3000 2650 3000 3000
Connection ~ 3000 3000
Wire Wire Line
	3000 3000 3500 3000
Wire Wire Line
	2000 2650 2000 3000
Connection ~ 2000 3000
Wire Wire Line
	2000 3000 1500 3000
Wire Wire Line
	1500 1700 1500 2000
Wire Wire Line
	1000 2000 1200 2000
Wire Wire Line
	1200 2000 1200 1500
Wire Wire Line
	1200 2500 1200 2000
Connection ~ 1200 2000
Wire Wire Line
	3800 1500 3800 2000
Wire Wire Line
	4000 2000 3800 2000
Connection ~ 3800 2000
Wire Wire Line
	3800 2000 3800 2500
Connection ~ 1500 2000
Wire Wire Line
	1500 2000 1500 2300
Wire Wire Line
	1500 2000 2000 2000
Connection ~ 2000 2000
Wire Wire Line
	2000 2000 2000 2350
Wire Wire Line
	2000 2000 2400 2000
Connection ~ 3000 2000
Wire Wire Line
	3000 2000 3000 2350
Wire Wire Line
	2600 2000 3000 2000
$Comp
L Transistor_BJT:BC107 T3
U 1 1 5FBE5FE5
P 3600 2500
F 0 "T3" H 3791 2500 50  0000 L CNB
F 1 "BC107" H 3791 2455 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-18-3" H 3800 2425 50  0001 L CNN
F 3 "http://www.b-kainka.de/Daten/Transistor/BC108.pdf" H 3600 2500 50  0001 L CNN
	1    3600 2500
	-1   0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:DIODE D1
U 1 1 5FBF9FA6
P 2000 1400
F 0 "D1" V 2000 1320 50  0000 R CNB
F 1 "DIODE" V 1955 1320 50  0001 R CNN
F 2 "" H 2000 1400 50  0001 C CNN
F 3 "~" H 2000 1400 50  0001 C CNN
F 4 "Y" H 2000 1400 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2000 1400 50  0001 L CNN "Spice_Primitive"
	1    2000 1400
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:BC107 T1
U 1 1 5FC67944
P 5400 1800
F 0 "T1" H 5590 1800 50  0000 L CNB
F 1 "BC107" H 5591 1755 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-18-3" H 5600 1725 50  0001 L CNN
F 3 "http://www.b-kainka.de/Daten/Transistor/BC108.pdf" H 5400 1800 50  0001 L CNN
	1    5400 1800
	1    0    0    -1  
$EndComp
$Comp
L Motor:Stepper_Motor_unipolar_6pin M?
U 1 1 5FC694A5
P 6500 1800
F 0 "M?" H 6688 1924 50  0001 L CNN
F 1 "Stepper_Motor_unipolar_6pin" H 6688 1833 50  0001 L CNN
F 2 "" H 6510 1790 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 6510 1790 50  0001 C CNN
	1    6500 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1500 5900 1500
Wire Wire Line
	7500 1500 7100 1500
$Comp
L Simulation_SPICE:DIODE D1
U 1 1 5FC70D6B
P 5900 1850
F 0 "D1" V 5900 1770 50  0000 R CNB
F 1 "DIODE" V 5855 1770 50  0001 R CNN
F 2 "" H 5900 1850 50  0001 C CNN
F 3 "~" H 5900 1850 50  0001 C CNN
F 4 "Y" H 5900 1850 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 5900 1850 50  0001 L CNN "Spice_Primitive"
	1    5900 1850
	0    -1   -1   0   
$EndComp
$Comp
L Simulation_SPICE:DIODE D2
U 1 1 5FC718AD
P 7100 1850
F 0 "D2" V 7100 1770 50  0000 R CNB
F 1 "DIODE" V 7055 1770 50  0001 R CNN
F 2 "" H 7100 1850 50  0001 C CNN
F 3 "~" H 7100 1850 50  0001 C CNN
F 4 "Y" H 7100 1850 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 7100 1850 50  0001 L CNN "Spice_Primitive"
	1    7100 1850
	0    -1   -1   0   
$EndComp
$Comp
L Simulation_SPICE:DIODE D4
U 1 1 5FC71C6D
P 7100 1200
F 0 "D4" V 7100 1120 50  0000 R CNB
F 1 "DIODE" V 7055 1120 50  0001 R CNN
F 2 "" H 7100 1200 50  0001 C CNN
F 3 "~" H 7100 1200 50  0001 C CNN
F 4 "Y" H 7100 1200 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 7100 1200 50  0001 L CNN "Spice_Primitive"
	1    7100 1200
	0    -1   -1   0   
$EndComp
$Comp
L Simulation_SPICE:DIODE D3
U 1 1 5FC72216
P 5900 1200
F 0 "D3" V 5900 1120 50  0000 R CNB
F 1 "DIODE" V 5855 1120 50  0001 R CNN
F 2 "" H 5900 1200 50  0001 C CNN
F 3 "~" H 5900 1200 50  0001 C CNN
F 4 "Y" H 5900 1200 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 5900 1200 50  0001 L CNN "Spice_Primitive"
	1    5900 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5900 1700 5900 1500
Connection ~ 5900 1500
Wire Wire Line
	5900 1500 6400 1500
Connection ~ 7100 1500
Wire Wire Line
	7100 1500 6600 1500
Wire Wire Line
	7100 1500 7100 1700
Wire Wire Line
	5900 2000 5900 2100
Wire Wire Line
	5900 2100 6500 2100
Wire Wire Line
	7100 2100 7100 2000
Wire Wire Line
	7500 2100 7100 2100
Connection ~ 7100 2100
Wire Wire Line
	5500 2100 5900 2100
Connection ~ 5900 2100
Wire Wire Line
	5900 950  6500 950 
$Comp
L power:+1V2 +U
U 1 1 5FC79709
P 6500 900
F 0 "+U" H 6588 937 50  0000 L CNB
F 1 "+1V2" H 6515 1073 50  0001 C CNN
F 2 "" H 6500 900 50  0001 C CNN
F 3 "" H 6500 900 50  0001 C CNN
	1    6500 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF GND?
U 1 1 5FC79A93
P 6500 2150
F 0 "GND?" H 6588 2113 50  0001 L CNN
F 1 "GNDREF" H 6588 2113 50  0001 L CNN
F 2 "" H 6500 2150 50  0001 C CNN
F 3 "" H 6500 2150 50  0001 C CNN
	1    6500 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2150 6500 2100
Connection ~ 6500 2100
Wire Wire Line
	6500 2100 7100 2100
Wire Wire Line
	6500 900  6500 950 
Connection ~ 6500 950 
Wire Wire Line
	6500 950  7100 950 
Wire Wire Line
	6500 1500 6500 950 
Text GLabel 5100 1800 0    50   Input ~ 0
A
Text GLabel 7900 1800 2    50   Input ~ 0
B
Wire Wire Line
	5500 2000 5500 2100
Wire Wire Line
	5500 1500 5500 1600
Wire Wire Line
	7500 1500 7500 1600
Wire Wire Line
	7500 2000 7500 2100
Wire Wire Line
	7800 1800 7900 1800
Wire Wire Line
	5100 1800 5200 1800
NoConn ~ 6200 1700
NoConn ~ 6200 1800
NoConn ~ 6200 1900
$Comp
L Transistor_BJT:BC107 T2
U 1 1 5FC67680
P 7600 1800
F 0 "T2" H 7791 1800 50  0000 L CNB
F 1 "BC107" H 7791 1755 50  0001 L CNN
F 2 "Package_TO_SOT_THT:TO-18-3" H 7800 1725 50  0001 L CNN
F 3 "http://www.b-kainka.de/Daten/Transistor/BC108.pdf" H 7600 1800 50  0001 L CNN
	1    7600 1800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3500 2000 3000 2000
Connection ~ 3500 2000
Wire Wire Line
	3500 2000 3500 2300
Wire Wire Line
	7100 950  7100 1050
Wire Wire Line
	7100 1350 7100 1500
Wire Wire Line
	5900 1050 5900 950 
Wire Wire Line
	5900 1350 5900 1500
$EndSCHEMATC
