/*
 * main.h
 *
 *  Created on: Nov 13, 2020
 *      Author: mark
 */

#ifndef MAIN_MAIN_H_
#define MAIN_MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

typedef enum{
	LOW = 0,
	HIGH
}t_state;

/**definialt tipus a a forgasirany tarolasara*/
typedef enum{
	CLOCKWISE = 0,
	COUNTER_CLOCKWISE
}t_direction;

/**definialt tipus a motor adatainak tarolasara*/
typedef struct{
    t_direction dir;
    uint32_t u32_step[64];
}t_motor;

/**definialt tipus a koordinatak tarolasara*/
typedef struct{
	uint8_t u8_ID;
    t_motor motor_x;
    t_motor motor_y;
    t_motor motor_z;
	uint32_t u32_reached_x;
	uint32_t u32_reached_y;
	uint32_t u32_reached_z;
	struct t_coordinate *next;
	struct t_coordinate *prev;
}t_coordinate;

/**definialt tipus a karakterek tarolasara*/
typedef struct {
	char name;
	uint8_t u8_weight;
	uint8_t u8_height;
	t_coordinate *zero_point; /**lista head-je*/
}t_character;

/**definialt tipus a vezerlesi adatoknak*/
typedef struct{
	uint32_t u32_poz_x;
	uint32_t u32_poz_y;
	uint32_t u32_poz_z;
	uint32_t u32_kar_offset;
	uint32_t u32_rel_offset;
	uint32_t u32_work_depth;
	uint32_t work_space_x;
	uint32_t work_space_y;
}t_control;

#endif /* MAIN_MAIN_H_ */
