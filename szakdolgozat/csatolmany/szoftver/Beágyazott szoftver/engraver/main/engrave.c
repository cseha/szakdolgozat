/*
 * engrave.c
 *
 *  Created on: Nov 13, 2020
 *      Author: mark
 */
#include <unistd.h>
#include "driver/gpio.h"
#include "hal/gpio_types.h"
#include "esp_log.h"
#include "esp_console.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "nvs.h"
#include "nvs_flash.h"

#include "main.h"
#include "engrave.h"

/**tömb a szenzorok gpio portjainak tárolására*/
gpio_num_t sensors[]= {
		GPIO_NUM_27, //x szenzor
		GPIO_NUM_14, //y szenzor
		GPIO_NUM_12  //z szenzor
};

/**tömb a motorok enable gpio portjainak tárolására*/
gpio_num_t motor_enable[]= {
		GPIO_NUM_19, //x motor enable
		GPIO_NUM_5,  //y motor enable
		GPIO_NUM_16  //z motor enable
};

/**tömb a motorok direction gpio portjainak tárolására*/
gpio_num_t motor_direction[]= {
		GPIO_NUM_18,  //x motor direction
		GPIO_NUM_17,  //y motor direction
		GPIO_NUM_4    //z motor direction
};

esp_err_t go_to_null(){

	t_state gpio_state[3];
	uint8_t status[3] = {0, 0, 0};
	/**beallitja a motorokat visszafele iranyba*/
	for(int i = 0; i < 3; i++){
		gpio_set_level(motor_direction[i], COUNTER_CLOCKWISE);
	}

	while(1){

		/**szenzorok állapotának beolvasása*/
		for(int i = 0; i < 3; i++){
			gpio_state[0] = gpio_get_level(sensors[i]);

			/**ha szenzor állapota nem logikai magas, lépünk*/
			if(gpio_state[i] != HIGH){
				gpio_set_level(motor_enable[i], HIGH);

			/**ha szenzor állapota nem logikai alacsony, megállunk*/
			}else{
				gpio_set_level(motor_enable[i], LOW);
				status[i]=1;
			}
		}

		/**ha mindhárom tengely nullába állt, kilépünk a ciklusból*/
		if(status[0] == 1 && status[1] == 1 && status[2] == 1){
			ESP_LOGI("engrave", "NULL point reached");
			break;
		}

		/**felfutó él generálása a motorvezérlők közösített STEP lábára*/
		gpio_set_level(GPIO_NUM_23, HIGH);
		usleep(50);//várakozunk 50 us-t -> 20 000 lépés/s = 200mm/s líneáris sebesség
		gpio_set_level(GPIO_NUM_23, LOW);
	}

	return ESP_OK;
}
