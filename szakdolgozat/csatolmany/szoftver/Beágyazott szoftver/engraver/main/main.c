/*
 * nvs.c
 *
 *  Created on: Nov 13, 2020
 *      Author: mark
 */

#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "esp_log.h"
#include "esp_console.h"
#include "esp_vfs_dev.h"
#include "driver/uart.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "esp_vfs_fat.h"
#include "nvs.h"
#include "nvs_flash.h"

#include "main.h"
#include "setup.h"
#include "data_save.h"
#include "engrave.h"

#define TAG "engraver"
#define PROMPT_STR "engraver"

TaskHandle_t console_task_handle;

static void console_task(){

	const char* prompt = LOG_COLOR_I PROMPT_STR "> " LOG_RESET_COLOR;

    printf("\n"
           "This is engraver MARK I, property of Harman International.\n"
           "Type 'help' to get the list of commands.\n"
           "Press TAB when typing command name to auto-complete.\n"
           "Pess Enter or Ctrl+C will terminate the console environment.\n"
    	   "Developer: Mark Cseharovszky. Contact email: Mark.Cseharovszky@harman.com\n");

    int probe_status = linenoiseProbe();
    if (probe_status) {
        printf("\n"
               "Your terminal application does not support escape sequences.\n"
               "Line editing and history features are disabled.\n"
               "On Windows, try using Putty instead.\n");
        linenoiseSetDumbMode(1);
#if CONFIG_LOG_COLORS

        prompt = PROMPT_STR "> ";
#endif //CONFIG_LOG_COLORS
    }

	while(true) {
		/*
		 * ENTER megnyomása esetén a begépelt paranbcsot betölti a stringbe
		 */
		char* line = linenoise(prompt);
		if (line == NULL) { /* üres sor vagy hiba*/
			break;
		}

		/* A parancs futtatása */
		int ret;
		esp_err_t err = esp_console_run(line, &ret);
		if (err == ESP_ERR_NOT_FOUND) {//ismeretlen parancs
			printf("Unrecognized command\n");
		} else if (err == ESP_ERR_INVALID_ARG) {// üres parancs

		} else if (err == ESP_OK && ret != ESP_OK) {//imsert parancs, hibás működés
			printf("Command returned non-zero error code: 0x%x (%s)\n", ret, esp_err_to_name(ret));
		} else if (err != ESP_OK) {//egyéb hiba
			printf("Internal error: %s\n", esp_err_to_name(err));
		}
		/* memória felszabadítása */
		linenoiseFree(line);
	}

	ESP_LOGE(TAG, "exiting console");
	esp_console_deinit();
}

static void initialize_nvs(void)
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK( nvs_flash_erase() );
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);
}

static void initialize_console(void)
{
    fflush(stdout);
    fsync(fileno(stdout));

    setvbuf(stdin, NULL, _IONBF, 0);

    esp_vfs_dev_uart_port_set_rx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM, ESP_LINE_ENDINGS_CR);
    esp_vfs_dev_uart_port_set_tx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM, ESP_LINE_ENDINGS_CRLF);

    /*UART port konfigurációja*/
    const uart_config_t uart_config = {
            .baud_rate = CONFIG_ESP_CONSOLE_UART_BAUDRATE,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .source_clk = UART_SCLK_REF_TICK,
    };
    /* Interupt beálltítása az UART portra*/
    ESP_ERROR_CHECK( uart_driver_install(CONFIG_ESP_CONSOLE_UART_NUM,
            256, 0, 0, NULL, 0) );
    ESP_ERROR_CHECK( uart_param_config(CONFIG_ESP_CONSOLE_UART_NUM, &uart_config) );

    esp_vfs_dev_uart_use_driver(CONFIG_ESP_CONSOLE_UART_NUM);

    /* Konzol inicializálása */
    esp_console_config_t console_config = {
            .max_cmdline_args = 8,
            .max_cmdline_length = 256,
#if CONFIG_LOG_COLORS
            .hint_color = atoi(LOG_COLOR_CYAN)
#endif
    };
    ESP_ERROR_CHECK( esp_console_init(&console_config) );

    /*Linenoise - a sorkezelő konfigurlálása*/
    linenoiseSetMultiLine(1);
    linenoiseSetCompletionCallback(&esp_console_get_completion);
    linenoiseSetHintsCallback((linenoiseHintsCallback*) &esp_console_get_hint);
    linenoiseHistorySetMaxLen(100);
    linenoiseAllowEmpty(true);

    xTaskCreate(console_task, "console", 2048, NULL, uxTaskPriorityGet(NULL), &console_task_handle);
}

void app_main(void)
{
    initialize_nvs();
    initialize_console();

    /* parancsok regisztrálása */
    esp_console_register_help_command();

    /*modulok regisztárlása*/
    register_data_save();
    register_setup();
    register_engrave();

}
