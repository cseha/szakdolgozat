/*
 * setup.c
 *
 *  Created on: Nov 13, 2020
 *      Author: mark
 */

#include "main.h"
#include "setup.h"

#include "esp_log.h"
#include "esp_console.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "nvs.h"
#include "nvs_flash.h"

#define KAROFF_KEY "karoff" //karakter offset kulcsa

/**szub-parancsargumentumok definialasa*/
static struct {
	struct arg_int *value;
	struct arg_end *end;
} set_karoff_args;

void set_runtime_data(uint32_t u32_kar_offset);

/**
 * karakter offset beállítási parancs érkezésére lefutó függvény,
 * mely az argumentumként kapott értéket elmenti a flash memóriába
 */
static esp_err_t set_karoff(int argc, char **argv)
{
    esp_err_t err;
    nvs_handle_t nvs;

    /**flash megnyítása írás-olvasás módban*/
    err = nvs_open("setup", NVS_READWRITE, &nvs);
    if (err != ESP_OK) {
        return err;
    }

    /**megadott érték mentése az arugmentumokból egy segédváltozóba*/
    uint32_t u32_kar_offset = set_karoff_args.value->ival[0];

    /**megadott érték mentése a flashbe kulcs alapján*/
    err = nvs_set_u32(nvs, KAROFF_KEY, u32_kar_offset);

    /**flash-ben történt válatoztatások véglegesítése*/
    if (err == ESP_OK) {
         err = nvs_commit(nvs);
         if (err == ESP_OK) {
             ESP_LOGI("setup", "Character offset set to %d", u32_kar_offset);
         }
     }

     /**a flasht minden használat után be kell zárni*/
     nvs_close(nvs);

     /**a RAM-ban lévő adatot is frissítjük*/
     set_runtime_data(u32_kar_offset);

     return err;
}

/**
 * A setup konzol létrehozása a megadott paraméterekkel
 * A main-ben kell meghívni
 */
void register_setup(){



	/**szub-parancs konfigurálása*/
	set_karoff_args.value = arg_litn("v", "value", "<value>", "value to be set");
	set_karoff_args.end = arg_end(0);

	/**konzol komponensek hozzarendelse esp_console_cmd_t-hez*/
	const esp_console_cmd_t set_karoff_cmd = {
			.command = "set_karoffset",
			.help = "sets distance between characters",
			.hint = NULL,
			.func = &set_karoff, /**a függvény, ami a parancs érkezésekor lefut*/
			.argtable = &set_karoff_args
	};

	/**konzol regisztrálása*/
	ESP_ERROR_CHECK(esp_console_cmd_register(&set_karoff_cmd));
}

