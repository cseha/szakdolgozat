#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define UNIT_PER_MM (100)
#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))

/**definialt tipus a a forgasirany tarolasara*/
typedef enum{
	CLOCKWISE = 0,
	COUNTER_CLOCKWISE
}t_direction;

/**definialt tipus a motor adatainak tarolasara*/
typedef struct{
    t_direction dir;
    uint32_t u32_step[64];
}t_motor;

/**definialt tipus a koordinatak tarolasara*/
typedef struct{
	uint8_t u8_ID;
    t_motor motor_x;
    t_motor motor_y;
    t_motor motor_z;
	uint32_t u32_reached_x;
	uint32_t u32_reached_y;
	uint32_t u32_reached_z;
	struct t_coordinate *next;
	struct t_coordinate *prev;
}t_coordinate;

/**definialt tipus a karakterek tarolasara*/
typedef struct {
	char name;
	uint8_t u8_weight;
	uint8_t u8_height;
	t_coordinate *zero_point; /**lista head-je*/
}t_character;

uint8_t init_coordinates(t_character *character);
uint8_t calculate_steps(t_character *character);
uint8_t write_character_to_file(t_character *character);

int main(int argc, char** argv){

	t_character character;
    bzero(&character, sizeof(character));

	if(init_coordinates(&character))
		return 1;

	if(calculate_steps(&character))
		return 1;

	if(write_character_to_file(&character))
		return 1;

	return 0;
}

/**
 * beolvasott fajlbol a koordinatak ertekeit eltarolja egy lancolt listaban
 * \param: a feltolteni kivant karakter valtozo cime
 * \retval: 0 ha sikeres
 * \retval: 1 ha sikertelen
 */
uint8_t init_coordinates(t_character *character){return 0;}

uint32_t get_y_step(t_coordinate *coordinate, uint32_t u32_pos){
	t_coordinate *coor_prev = coordinate->prev;

	/**seged valtozok*/
	uint32_t u32_x1 = MAX(coordinate->u32_reached_x, coor_prev->u32_reached_x);
	uint32_t u32_y1 = MAX(coordinate->u32_reached_y, coor_prev->u32_reached_y);
	uint32_t u32_x0 = MIN(coordinate->u32_reached_x, coor_prev->u32_reached_x);
	uint32_t u32_y0 = MIN(coordinate->u32_reached_y, coor_prev->u32_reached_y);
	double i;

    /**linaris interpolacio*/
    double tg = (double)(u32_y1-u32_y0) / (double)(u32_x0-u32_x0);
    double y = (double)u32_y0 + (double)(u32_x0+u32_pos - u32_x0) * tg;
    double f = modf(y, &i); /**tort rész megszerzese*/

    if(i < 0.5){
        return 0; /**ha a tortresz kisebb mint 0.5, nem lep a motor*/
    }

	return 1;
}

/**
 * linearis interpolacio ket pont kozott
 * \param: cel koordinatara mutato pointer
 */
uint8_t linear_interpolation(t_coordinate *coordinate){
	if(coordinate == NULL){
		printf("t_coordinate does not exist");
		return 1;
	}

    /**x es y tavolsag kiszamolasa*/
	t_coordinate *coor_prev = coordinate->prev;
	int16_t i16_x_distance = coordinate->u32_reached_x - coor_prev->u32_reached_x;
	int16_t i16_y_distance = coordinate->u32_reached_y - coor_prev->u32_reached_y;

    /**x es y motor forgasiranyanak beallitasa*/
	(i16_x_distance > 0) ? (coordinate->motor_x.dir = CLOCKWISE) : (coordinate->motor_x.dir = COUNTER_CLOCKWISE);
	(i16_y_distance > 0) ? (coordinate->motor_y.dir = CLOCKWISE) : (coordinate->motor_y.dir = COUNTER_CLOCKWISE);

    /**x es y motor lepesszamainak kiszamolasa*/
	int16_t i16_x_steps = abs(i16_x_distance) * UNIT_PER_MM;
	int16_t i16_y_steps = abs(i16_y_distance) * UNIT_PER_MM;
	uint32_t *mot_al_step;
	uint32_t *less_stepping;

    /**a ket koordinata kozott a tobb lepest megtevo motor meghatarozasa*/
	if(i16_x_distance > i16_y_distance){
		mot_al_step = coordinate->motor_x.u32_step;
		less_stepping = coordinate->motor_y.u32_step;
	}else{
		mot_al_step = coordinate->motor_y.u32_step;
		less_stepping = coordinate->motor_x.u32_step;
	}

    /**ciklus bit-ek shiftelesere a lepeseket tarolo tombokbe*/
	for(uint32_t i = 0; i < MAX(i16_x_steps, i16_y_steps); i++){

        /**a tobbbet levo motorba mindig shiftel*/
		mot_al_step[i/32] |= 1 << (i%32);

		/**a lineris interpolacioval kiszamolja mikor lepjen a kevesebbet lepo motor*/
		if(get_y_step(coordinate, i*UNIT_PER_MM)){
			less_stepping[i/32] |= 1 << (i%32);
		}
	}
	return 0;
}

/**
 * a leptetomotorok lepeseinek kiszamolasa ket koordinata kozott
 * \param: a feltolteni kivant karakter valtozo cime
 */
uint8_t calculate_steps(t_character *character){

    /**NULL ellenorzes*/
	if(character->zero_point == NULL){
		printf("empty list\n");
		return 1;
	}

    /**seged pointerek a lista olvasasahoz*/
	t_coordinate *coor_current = character->zero_point;
	t_coordinate *coor_next = coor_current->next;

	/**amig a listaban van elem*/
	while(coor_next){

        /**ha nincs z tengely menti elmozdulas*/
		if(coor_next->u32_reached_z != coor_current->u32_reached_z){

            /**linaris interpolacio vegrehajtasa a kovetkezo koordinatahoz*/
            if(linear_interpolation(coor_next)){
                return 1;
            }
		}
		/**z tengely menti elmozdulas eseten a z motor forgas irany beallitasa*/
		else{
			(coor_next->u32_reached_z - coor_current->u32_reached_z > 0) ?
					(coor_next->motor_z.dir = CLOCKWISE):
					(coor_next->motor_z.dir = COUNTER_CLOCKWISE);
		}

        /**kovetekezo lista elemre lepes*/
		coor_next = coor_next->next;
	}

	return 0;
}

/**
 * beolvasott fajlbol a koordinatak ertekeinek eltarolasa egy lancolt listaban
 * \param: a feltolteni kivant karakter valtozo cime
 * \retval: 0 ha sikeres
 * \retval: 1 ha sikertelen
 */
uint8_t write_character_to_file(t_character *character){return 0;}


